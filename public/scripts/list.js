window.onload = function(){
    // Users
    var AddUserBtn = document.getElementById('AddUserBtn');
    var AddUserFormDiv = document.getElementById('UserAdd');
    var ConfirmUserBtn = document.getElementById('ConfirmUser');
    var CancelUserBtn = document.getElementById('CancelUser');
    var XCancelUserBtn = document.getElementById('XCancelUser');
    var UserList = document.querySelector('.UserList');

    var showUserForm = 1;

    AddUserBtn.addEventListener("click", function(){
        console.log('showUsers = ' + showUserForm);
        if (showUserForm === 1) {
            AddUserFormDiv.style.display = "block";
            showUserForm = 0;}
        else {
            AddUserFormDiv.style.display = "none";
            showUserForm = 1;
        }
    });

    CancelUserBtn.addEventListener("click", function(){
        AddUserFormDiv.style.display = "none";
        showUserForm = 1;
    });
    XCancelUserBtn.addEventListener("click", function(){
        AddUserFormDiv.style.display = "none";
        showUserForm = 1;
    });

    ConfirmUserBtn.addEventListener("click", (e) => addToList(1));

    UserList.addEventListener("click", (e) => removeFromList(e, 1));
    UserList.addEventListener("click", (e) => editList(e, 1));

    //Units
    var AddUnitBtn = document.getElementById('AddUnitBtn');
    var AddUnitFormDiv = document.getElementById('UnitAdd')
    var ConfirmUnitBtn = document.getElementById('ConfirmUnit');
    var CancelUnitBtn = document.getElementById('CancelUnit');
    var XCancelUnitBtn = document.getElementById('XCancelUnit');
    var UnitList = document.querySelector('.UnitList');


    var showUnitForm = 1;

    AddUnitBtn.addEventListener("click", function(){
        console.log('showUnits = ' + showUnitForm);
        if (showUnitForm === 1) {
            AddUnitFormDiv.style.display = "block";
            showUnitForm = 0;}
        else {
            AddUnitFormDiv.style.display = "none";
            showUnitForm = 1;
        }
    });


    CancelUnitBtn.addEventListener("click", function(){
        AddUnitFormDiv.style.display = "none";
        showUnitForm = 1;
    });
    XCancelUnitBtn.addEventListener("click", function(){
        AddUnitFormDiv.style.display = "none";
        showUnitForm = 1;
    });

    ConfirmUnitBtn.addEventListener("click", (e) => addToList(2));

    UnitList.addEventListener("click", (e) => removeFromList(e, 2));
    UnitList.addEventListener("click", (e) => editList(e, 2));

    var startUserId = 0;

    var Users = [];
    var Units = [];

    function jsonStructure(fullname,password,description,uid = undefined){
        this.fullname = fullname;
        this.password = password;
        this.description = description;
        if (uid) {
            this.uid = uid;
        }
        else {
            this.uid = startUserId;
            startUserId += 1;
        }
    }

    function addToList(storage, id=undefined){
        console.log("Add obj w id " + id);
        const LS = (storage === 1 ? "User" : "Unit") + "List";
        const d = storage === 1 ? {
            name: id ? document.getElementById('Fullname' + LS + id).value : document.getElementById('UserFullname').value,
            password: id ? document.getElementById('Password' + LS + id).value : document.getElementById('UserPassword').value,
            description: id ? document.getElementById('Descr' + LS + id).value : document.getElementById('UserDescr').value,
            uid: id ? id : null
        } : {
            name: id ? document.getElementById('Fullname' + LS + id).value : document.getElementById('UnitFullname').value,
            password: id ? document.getElementById('Password' + LS + id).value : document.getElementById('UnitPassword').value,
            description: id ? document.getElementById('Descr' + LS + id).value : document.getElementById('UnitDescr').value,
            uid: id ? document.getElementById('Id' + LS + id).value : document.getElementById('UserId').value
        };
        console.log("name is " + d.name);
        console.log("password is " + d.password);
        console.log("description is " + d.description);
        console.log("uid is " + d.uid);
        var isNull = d.name!='' && d.password!='' && d.description!='' && ( storage === 1 ? true : d.uid !='');
        console.log("isNull is " + isNull);
        if(isNull){
            if (id != null){
                if (storage === 1) {
                    console.log("We update!");
                    Users[id].fullname = d.name;
                    Users[id].password = d.password;
                    Users[id].description = d.description;
                    localStorage['UserList'] = JSON.stringify(Users);
                    document.querySelector('.Modal').style.display = "none";
                } else {
                    Units[id].fullname = d.name;
                    Units[id].password = d.password;
                    Units[id].description = d.description;
                    Units[id].uid = d.uid;
                    localStorage['UnitList'] = JSON.stringify(Units);
                    document.querySelector('.Modal').style.display = "none";
                }
            }
            else {
                console.log("We add!");
                var obj = new jsonStructure(d.name, d.password, d.description, d.uid);
                if (storage === 1) {
                    Users.push(obj);
                    localStorage['UserList'] = JSON.stringify(Users);
                    AddUserFormDiv.style.display = "none";
                    showUserForm = 1;
                } else {
                    Units.push(obj);
                    localStorage['UnitList'] = JSON.stringify(Units);
                    AddUnitFormDiv.style.display = "none";
                    showUnitForm = 1;
                }
            }
            clearForm(storage);
            showList(storage);
        }
    }

    function removeFromList(e, storage){
        if(e.target.classList.contains('delbutton')){
            var remID = e.target.getAttribute('data-id');
            const arr = storage === 1 ? Users : Units;
            const LS = (storage === 1 ? "User" : "Unit") + "List";
            arr.splice(remID,1);
            localStorage[LS] = JSON.stringify(arr);
            showList(storage);
        }
    }

    var showEditFrom = 1;

    function editList(e, storage){
        console.log("in edit");
        if(e.target.classList.contains('editbutton')){
            console.log("in edit2");
            var id = e.target.getAttribute('data-id');
            const LS = (storage === 1 ? "User" : "Unit") + "List";
            console.log("editID in editList func is " + id);
            var EditFormDiv = document.getElementById('Edit' + LS + id);
            var ConfirmEditBtn = document.getElementById('ConfirmEdit' + LS + id);
            var CancelEditBtn = document.getElementById('CancelEdit' + LS + id);

            console.log('Edit = ' + showEditFrom);
            if (showEditFrom === 1) {
                EditFormDiv.style.display = "block";
                showEditFrom = 0;}
            else {
                EditFormDiv.style.display = "none";
                showEditFrom = 1;
            }

            console.log('Display is ' + EditFormDiv.style.display);

            CancelEditBtn.addEventListener("click", function(){
                EditFormDiv.style.display = "none";
                showEditFrom = 1;
            });

            ConfirmEditBtn.addEventListener("click", (e) => {
                EditFormDiv.style.display = "none";
                showEditFrom = 1;
                addToList(storage, id);});
        }
    }


    function clearForm(storage){
        var formFields = document.querySelectorAll(`.container_${storage} .formFields`);
        for(var i in formFields){
            formFields[i].value = '';
        }
    }

    function showList(storage){
        const LS = (storage === 1 ? "User" : "Unit") + "List";
        if(localStorage[LS] === undefined){
            localStorage[LS] = '';
        } else {
            if (storage === 1) {
                Users = JSON.parse(localStorage['UserList']);
                UserList.innerHTML = '';
            } else {
                Units = JSON.parse(localStorage['UnitList']);
                UnitList.innerHTML = '';
            }

            const arr = storage === 1 ? Users : Units;
            for(var n in arr){
                var str = '<div class="row mx-2 p-2 border-bottom">';
                        str += '<div class="col">' + arr[n].fullname + '</div>';
                    if (storage === 2){
                        str += '<div class="col">' + arr[n].uid + '</div>';
                    }
                    str += '<div class="col">' + arr[n].password + '</div>';
                    str += '<div class="col">' + arr[n].description + '</div>';
                    str += '<div class="col-1"><button class="editbutton btn btn-sm btn-outline-success w-75 " data-id="' + n + '">Edit</button></div>';
                    str += '<div class="col-1"><button class="delbutton btn btn-sm btn-outline-danger" data-id="' + n + '">Delete</button></div>';
                    str += '</div>';

                    str += '</div>';
                    str += '<div class="Modal" id="Edit' + LS + n + '">'
                    str += '<div class="Modal-content">';
                    str += '<div class="p-1"><button type="button" class="close" aria-label="Close" id="CancelEdit' + LS + n + '"><span aria-hidden="true">&times;</span> </button><br></div><form class="p-2">'
                    str += '<div class="form-group row"><label for="fullname" class="col-sm-2 col-form-label">Name</label><div class="col-sm-10"><input type="text" id="Fullname' + LS + n + '" class="formFields form-control" value=' + arr[n].fullname + '></div></div>';
                    if (storage === 2){
                        str += '<div class="form-group row"><label for="id" class="col-sm-2 col-form-label">User ID</label><div class="col-sm-10"><input type="text" id="Id' + LS + n + '" class="formFields form-control" value=' + arr[n].uid + '></div></div>';}

                    str += '<div class="form-group row"><label for="password" class="col-sm-2 col-form-label">Secret</label><div class="col-sm-10"><input type="text" id="Password' + LS + n + '" class="formFields form-control" value=' + arr[n].password + '></div></div>';
                    str += '<div class="form-group row"><label for="description" class="col-sm-2 col-form-label">Descr</label><div class="col-sm-10"><textarea name="text" id="Descr' + LS + n + '" cols="30" rows="10" class="formFields form-control">' + arr[n].description + '</textarea></div></div>';
                    str += '<div class="d-flex flex-row-reverse"><button id="ConfirmEdit' + LS + n + '" class="btn btn-success mx-1">Confirm</button><button id="CancelEdit' + LS + n + '" class="btn btn-outline-danger mx-1">Cancel</button>';
                    str += '</div></form></div>';

                if (storage === 1) {
                    UserList.innerHTML += str;
                } else {
                    UnitList.innerHTML += str;
                }
            }
        }
    }

    showList(1);
    showList(2);
}
