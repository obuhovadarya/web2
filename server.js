const
  express    = require('express'),
  urlencoded = require('body-parser'),
  cors       = require('cors'),
  app        = express(),
  port       = 8080;

app.use(urlencoded({ useUnifiedTopology: true  }))
app.use(cors())
app.use(express.static(__dirname + "/public"))

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html")
})

app.listen(port, () => {
  console.log(`Listening port ${port}`)
})
